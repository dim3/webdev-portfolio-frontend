# webdev-portfolio-frontend

A website built using Nuxt and Three.js to showcase projects.

The server folder has a simple Rest API. I should move the JSON and HTML there to a database at some point, so it's easier to edit the projects info.

### How to test the build for Netlify

```bash
pnpm run build
npx serve dist
```

Then open http://localhost:3000/

### TODO

- Refactor server so the data is served not served through local JSON and HTML.
- Use Strapi for the backend?

### Notes

Make stylelint work with Vue files and prevent 'Unknown word CssSyntaxError' error:

- https://github.com/ota-meshi/stylelint-config-standard-vue
