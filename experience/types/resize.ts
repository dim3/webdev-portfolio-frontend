export type ResizeEventDetail = {
  width: number;
  height: number;
  pixelRatio: number;
};
