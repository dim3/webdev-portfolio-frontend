export type TimeTickEventDetail = {
  elapsed: number;
  elapsedMs: number;
  delta: number;
  deltaMs: number;
};
