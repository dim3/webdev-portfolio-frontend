export const isObjectEmpty = (o: Object) => {
  return o && Object.keys(o).length === 0;
};
